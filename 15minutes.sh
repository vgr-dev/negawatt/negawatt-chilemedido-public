#!/bin/bash
# Copyright (C) 2019-2021 VGR SpA - All Rights Reserved
# Copyright (C) 2019-2021 Ingenieria y Energia NegaWatt Ltda. - All Rights Reserved
#
# Unauthorized copying of this file, via any medium is strictly prohibited.
# This code is propietary and confidential.
# You may not use this file except in compliance with the Copyright holders.
# Any kind of misuse of this file and intellectual properties may incur in legal prosecution.
#
# Author: Victor Gonzalez <victor@vgr.cl>

# resolve folder location relative to this script
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

# perform 15 minute 1 data point sent, except at midnight and at hourly times
cd "$DIR"
if [[ $(date +%H:%M:%S) > "00:14:59" ]] && [[ $(date +%M) -ge "15" ]];
then
  timeout -k 10 12m ./data_handling script_15minutes >> "$DIR/15minutes.log" 2>&1
  timeout -k 10 2m ./monitoring >> "$DIR/monitoring.log" 2>&1
fi

# perform hourly task (1 data point + 200 first in queue)
if [[ $(date +%H:%M:%S) > "00:14:59" ]] && [[ $(date +%M) -ge "00" ]] && [[ $(date +%M) < "15" ]];
then
  timeout -k 10 12m ./data_handling script_hourly >> "$DIR/hourly.log" 2>&1
  timeout -k 10 2m ./monitoring >> "$DIR/monitoring.log" 2>&1
fi

# reboot devices on monday at 8am
if [[ $(date +%H:%M:%S) > "08:00:00" ]] && [[ $(date +%H:%M:%S) < "08:15:00" ]] && [[ $(date +%u) -eq 1 ]]; then sudo reboot now; fi
