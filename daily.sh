#!/bin/bash
# Copyright (C) 2019-2021 VGR SpA - All Rights Reserved
# Copyright (C) 2019-2021 Ingenieria y Energia NegaWatt Ltda. - All Rights Reserved
#
# Unauthorized copying of this file, via any medium is strictly prohibited.
# This code is propietary and confidential.
# You may not use this file except in compliance with the Copyright holders.
# Any kind of misuse of this file and intellectual properties may incur in legal prosecution.
#
# Author: Victor Gonzalez <victor@vgr.cl>

# resolve folder location relative to this script
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

# perform operations and save logs
cd "$DIR"
timeout -k 10 14m ./data_handling script_daily >> "$DIR/daily.log" 2>&1

# logrotate files
logrotate --force "$DIR/logrotate.conf" -s "$DIR/logrotate.state"

# upgrade software if possible
"$DIR/upgrade.sh"

# send monitoring data after all the procedures
#timeout -k 10 2m ./monitoring >> "$DIR/monitoring.log" 2>&1
