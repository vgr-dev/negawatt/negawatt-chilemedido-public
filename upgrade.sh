#!/bin/bash
# Copyright (C) 2019-2021 VGR SpA - All Rights Reserved
# Copyright (C) 2019-2021 Ingenieria y Energia NegaWatt Ltda. - All Rights Reserved
#
# Unauthorized copying of this file, via any medium is strictly prohibited.
# This code is propietary and confidential.
# You may not use this file except in compliance with the Copyright holders.
# Any kind of misuse of this file and intellectual properties may incur in legal prosecution.
#
# Author: Victor Gonzalez <victor@vgr.cl>

# resolve folder location relative to this script
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"

# move to correct folder
cd "$DIR"

# Set git-config values known to fix git errors
# Set user
git config --global user.email "chilemedido@negawatt.cl"
git config --global user.name "Chilemedido Negawatt"
# Line endings
git config core.eol lf
git config core.autocrlf false
# zeroPaddedFilemode fsck errors
git config fsck.zeroPaddedFilemode ignore
git config fetch.fsck.zeroPaddedFilemode ignore
git config receive.fsck.zeroPaddedFilemode ignore
# autostash on rebase
git config rebase.autoStash true
rm -rf .git/rebase-apply

last_commit=$(git rev-parse HEAD)
if git pull --rebase --stat origin master; then
  # Check if it was really updated or not
  if [[ "$(git rev-parse HEAD)" = "$last_commit" ]]; then
    echo "[$(date +'%Y-%m-%d %T')] No updates" >> upgrade.log
  else
    echo "[$(date +'%Y-%m-%d %T')] Updated from $last_commit to $(git rev-parse HEAD)" >> upgrade.log
  fi
fi
